import { createApp } from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import { loadFonts } from './plugins/webfontloader'

import NotFound from "./components/NotFound.vue";
import Calendar from "./components/Calendar-element.vue";
import Dashboard from "./components/Dashboard-element.vue";
import Subbands from "./components/real-time/Subbands-graph.vue";
import Beamlets from "./components/real-time/Beamlets-graph.vue";
import { createRouter, createWebHashHistory } from "vue-router";

loadFonts()

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    { path: "/", name: "Base", component: null },
    { path: "/calendar", name: "Calendar", component: Calendar },
    { path: "/dashboard", name: "Dashboard", component: Dashboard },
    { path: "/subbands", name: "Subbands", component: Subbands },
    { path: "/beamlets", name: "Beamlets", component: Beamlets },
    { path: "/:pathMatch(.*)*", name: "Not Found", component: NotFound },
  ],
});

createApp(App)
  .use(vuetify)
	.use(router)
  .mount('#app')
