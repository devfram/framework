import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-beamlets',
  templateUrl: './beamlets.component.html',
  styleUrls: ['./beamlets.component.css'],
})
export class BeamletsComponent implements OnInit {
  @Input() styles: String = new String();
  @Input() xAxis = true;
  @Input() toolbox = true;

  options: any;
  constructor() {}

  ngOnInit(): void {
    const xData: number[] = new Array(100);
    const yData: number[] = new Array(100);
    const yData2: number[] = new Array(100);
    let point = 100;
    let point2 = 75;

    for (let i = 0; i < 100; i++) {
      xData[i] = i;
      yData[i] = point;
      point += Math.floor((Math.random() - 0.5) * 100);
      yData2[i] = point2;
      point2 += Math.floor((Math.random() - 0.5) * 100);
    }

    this.options = {
      dataZoom: {
        type: 'inside',
      },
      title: {
        left: 'center',
        text: 'Subband data',
        textStyle: {
          color: '#CFD3DC',
        },
      },
      grid: {
        top: 40,
        left: 35,
        right: 10,
        bottom: 0,
      },
      xAxis: {
        type: 'category',
        data: xData,
        axisLabel: {
          show: this.xAxis,
        },
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          data: yData,
          type: 'line',
        },
        {
          data: yData2,
          type: 'line',
        },
      ],
      toolbox: {
        iconStyle: {
          borderColor: '#A3A6AD',
        },
        left: '2%',
        orient: 'vertical',
        itemSize: 25,
        top: 55,
        feature: {
          dataZoom: {
            yAxisIndex: 'none',
          },
          restore: {},
        },
        show: this.toolbox,
      },
    };
  }
}
