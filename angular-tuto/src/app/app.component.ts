import { Component, AfterViewInit } from "@angular/core";

@Component({
  selector: "guess-num",
  template: `
    <div id="drawer">
      <sl-drawer placement="start" label="Menu" class="drawer-overview">
        <sl-button slot="footer" variant="primary">Close</sl-button>
        <sl-menu-item
          routerLinkActive="activebutton"
          ariaCurrentWhenActive="page"
          routerLink="/regles"
          value="regles"
          >Regles</sl-menu-item
        >
        <sl-divider></sl-divider>
        <sl-menu-item
          routerLinkActive="activebutton"
          ariaCurrentWhenActive="page"
          routerLink="/jeu"
          value="jeu"
          >Jeu</sl-menu-item
        >
      </sl-drawer>

      <sl-icon-button
        id="menu"
        name="list"
        label="Menu"
        style="font-size: 2.5rem"
      ></sl-icon-button>
    </div>
    <router-outlet></router-outlet>
  `,
  styles: [
    `
      .activebutton {
        background-color: grey;
      }
      #menu {
        position: fixed;
      }
      #main {
        margin: auto;
        width: 50%;
      }

      .menu-item {
        width: 100%;
        margin-left: -22px;
      }

      :disabled {
        cursor: not-allowed;
      }

      sl-alert {
        position: absolute;
        display: inline;
        padding-left: 50%;
        width: 1200px;
      }
      sl-input {
        margin: 5px;
        display: inline-block;
        width: 80%;
      }
    `,
  ],
})
export class AppComponent implements AfterViewInit {
  constructor() {}
  ngAfterViewInit(): void {
    const drawer: any = document.querySelector(".drawer-overview");
    console.log("drawer= " + drawer);

    const closeButton: any = drawer.querySelector(
      'sl-button[variant="primary"]'
    );

    const openButton = drawer.nextElementSibling;
    openButton!.addEventListener("click", () => (drawer.open = true));
    closeButton!.addEventListener("click", () => (drawer.open = false));
  }
}
