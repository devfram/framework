import { html, css, LitElement } from 'lit';

export class GuessNum extends LitElement {
	static styles = css`
    button { background-color: transparent; 
        border: solid; 
        border-color:#505050; 
        border-width:2px;
        cursor: pointer;
    }
    :disabled {
        cursor: not-allowed;
    }`;

	static properties = {
		max: { type: Number },
		min: {},
		nbEssaisInit: {},
		nbEssais: {},
		histo: {},
		randNum: {},
		pos: {},
		status: {},
	};

	constructor() {
		super();
		this.max = 100;
		this.min = 1
		this.nbEssais = 10
		this.nbEssaisInit = this.nbEssais
		this.histo = "Guess précédents : "
		this.randNum = Math.floor(Math.random() * this.max + this.min);
		console.log(this.randNum);
	}



	render() {

		return html`
						<input type="number" ?disabled=${this.status==="win" || this.status==="loose"} name="guess" id="guess"
							placeholder="Entrez votre guess">
						<button id="valider" ?disabled=${this.status==="win" || this.status==="loose"} @click=${this.takeGuess}>Guess
							!</button>
						<button id="reset" @click=${this.reset}>Reset</button>
						<br />
						<h3 ?hidden=${this.status !="miss"} id="pos">${this.pos}</h3>
						<p ?hidden=${this.satus===null} id="histo">${this.histo}</p>
						<p ?hidden=${this.status !="loose"} id="loose">Dommage, le nombre était ${this.randNum}</p>
						<p ?hidden=${this.status !="win"} id="win">Bravo !</p>
						<p>${this.nbEssais} Essais restants</p>
            `
	}

	takeGuess(event) {
		const guess = parseInt(this.shadowRoot.getElementById("guess").value);
		if (!guess) {
			alert("Rentrez une valeur");
			return;
		}
		console.log(guess);

		if (guess === this.randNum) {
			this.status = "win";
			console.log("victoire");
			return;
		}
		if (guess < this.randNum) {
			this.pos = "Trop bas";
		} else {
			this.pos = "Trop haut";
		}
		this.status = "miss";
		this.histo += guess + " ";
		this.nbEssais--;
		if (this.nbEssais === 0) {
			this.status = "loose";
		}

		this.shadowRoot.getElementById("guess").value = null;
	}

	reset() {
		this.randNum = Math.floor(Math.random() * this.max + this.min);
		this.shadowRoot.getElementById("guess").value = null;
		console.log(this.randNum);
		this.status = null;
		this.nbEssais = this.nbEssaisInit;
		this.histo = "Guess précédents : ";
	}
}
customElements.define('guess-num', GuessNum);