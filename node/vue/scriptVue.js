
import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'

import App from './app.vue'

import Jeu from './Jeu.vue';
import Regles from './Regles.vue';
import NotFound from './NotFound.vue';

const router = createRouter({
	history: createWebHashHistory(),
	routes: [
		{ path: '/', name: 'Regles', component: Regles },
		{ path: '/jeu', name: 'Jeu', component: Jeu },
		{ path: '/:pathMatch(.*)*', name: 'Not Found', component: NotFound }
	],
})

createApp(App).use(router).mount('#app')