import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubbandsComponent } from './subbands.component';

describe('SubbandsComponent', () => {
  let component: SubbandsComponent;
  let fixture: ComponentFixture<SubbandsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubbandsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SubbandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
