import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-subbands',
  templateUrl: './subbands.component.html',
  styleUrls: ['./subbands.component.css'],
})
export class SubbandsComponent implements OnInit {
  @Input() styles: String = new String();
  @Input() xAxis = true;
  @Input() toolbox = true;
  options: any;
  constructor() {}

  ngOnInit(): void {
    const xData: number[] = new Array(1000);
    const yDatas: Array<Array<number>> = new Array(100);
    let point = 100;

    for (let i = 0; i < 10; i++) {
      const yData: number[] = new Array(1000);
      for (let j = 0; j < 1000; j++) {
        yData[j] = point;
        point += Math.floor((Math.random() - 0.5) * 50);
        point = Math.abs(point);
        xData[j] = j;
      }
      yDatas[i] = yData;
      point = 100;
    }

    this.options = {
      dataZoom: {
        type: 'inside',
      },
      title: {
        left: 'center',
        text: 'Subband data',
        textStyle: {
          color: '#CFD3DC',
        },
      },
      grid: {
        top: 40,
        left: 35,
        right: 10,
        bottom: 0,
      },
      xAxis: {
        type: 'category',
        data: xData,
        axisLabel: {
          show: true,
        },
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          data: yDatas[0],
          type: 'line',
        },
        {
          data: yDatas[1],
          type: 'line',
        },
        {
          data: yDatas[2],
          type: 'line',
        },
        {
          data: yDatas[3],
          type: 'line',
        },
        {
          data: yDatas[4],
          type: 'line',
        },
        {
          data: yDatas[5],
          type: 'line',
        },
        {
          data: yDatas[6],
          type: 'line',
        },
      ],
      toolbox: {
        iconStyle: {
          borderColor: '#A3A6AD',
        },
        left: '2%',
        orient: 'vertical',
        itemSize: 25,
        top: 55,
        feature: {
          dataZoom: {
            yAxisIndex: 'none',
          },
          restore: {},
        },
        show: true,
      },
    };
  }
}
