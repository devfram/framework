import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import react from '@vitejs/plugin-react'
// https://vitejs.dev/config/
export default defineConfig({
    server: {
        host: '0.0.0.0',
        port: 8080
    },
    plugins: [vue(),react()],
    build: {
        lib: {
            entry: 'app/scriptLit.js',
            formats: ['es']
        },
        rollupOptions: {
            external: /^lit/
        }
    }
})
