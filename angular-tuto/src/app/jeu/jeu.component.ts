import { Component, AfterViewInit } from "@angular/core";

@Component({
  selector: "jeu-element",
  template: `
    <div class="jeu">
      <sl-alert variant="warning" duration="3000" closable>
        <sl-icon slot="icon" name="exclamation-triangle"></sl-icon>
        <strong>Rentrez une valeur conforme</strong><br />
        Nombre entre 1 et 100
      </sl-alert>
      <sl-input
        autofocus
        pill
        type="number"
        placeholder="ex : 53"
        label="Votre guess"
        [value]="guessValue"
        [disabled]="status === 'win' || status === 'loose'"
        (input)="onInput($event)"
        (keyup)="isEnter($event)"
      ></sl-input>

      <sl-button-group>
        <sl-button
          variant="success"
          outline
          [disabled]="status === 'win' || status === 'loose'"
          (click)="onGuess()"
        >
          Guess !
        </sl-button>
        <sl-button variant="warning" outline (click)="reset()">Reset</sl-button>
      </sl-button-group>
      <br />
      <br />
      <h3 [hidden]="status != 'miss'">{{ pos }}</h3>
      <p [hidden]="status === '' || histo === 'Guess précédents : '">
        {{ histo }}
      </p>
      <p [hidden]="status != 'loose'">Dommage, le nombre était {{ randNum }}</p>
      <p [hidden]="status != 'win'">Bravo !</p>
      <sl-rating
        id="rate"
        class="rating-hearts"
        style="--symbol-color-active: #ff4136"
        readonly
        max="10"
        [value]="nbEssais"
      ></sl-rating>
    </div>
  `,
  styles: [
    `
      .jeu {
        margin: auto;
        width: 50%;
      }
      .line {
        white-space: nowrap;
      }

      sl-alert {
        position: absolute;
        display: inline;
        padding-left: 50%;
        width: 1200px;
      }
      sl-input {
        margin: 5px;
        display: inline-block;
        width: 80%;
      }
    `,
  ],
})
export class JeuComponent implements AfterViewInit {
  pos = "";
  histo = "Guess précédents : ";
  randNum = Math.floor(Math.random() * 100);
  guessValue = "";
  status: string;
  nbEssais = 10;
  constructor() {
    console.log(this.randNum);
  }
  ngAfterViewInit(): void {
    const rating: any = document.querySelector(".rating-hearts");
    rating.getSymbol = () => '<sl-icon name="heart-fill"></sl-icon>';
  }

  onInput(e: any) {
    this.guessValue = e.target.value;
    console.log(e);
  }

  onGuess() {
    if (!this.guessValue) {
      const alert: any = document.querySelector(`sl-alert`);
      alert.show();
      return;
    }

    if (parseInt(this.guessValue) === this.randNum) {
      this.status = "win";
      console.log("victoire");
      return;
    }
    if (parseInt(this.guessValue) < this.randNum) {
      this.pos = "Trop bas";
    } else {
      this.pos = "Trop haut";
    }
    this.status = "miss";
    this.histo += this.guessValue + " ";
    this.nbEssais--;
    if (this.nbEssais === 0) {
      this.status = "loose";
    }

    this.guessValue = "";
  }
  reset() {
    this.guessValue = "";
    this.randNum = Math.floor(Math.random() * 100 + 1);
    this.status = "";
    this.nbEssais = 10;
    this.histo = "Guess précédents : ";
    console.log(this.randNum);
  }
  isEnter(event: any) {
    if (event.code === "Enter") {
      this.onGuess();
    }
  }
}
