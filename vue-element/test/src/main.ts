import { createApp } from "vue";
import { createPinia } from "pinia";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
import App from "./App.vue";
import NotFound from "./components/NotFound.vue";
import Calendar from "./components/Calendar-element.vue";
import Dashboard from "./components/Dashboard-element.vue";
import { createRouter, createWebHashHistory } from "vue-router";

import "./assets/main.css";

const app = createApp(App);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    { path: "/", name: "Base", component: null },
    { path: "/calendar", name: "Calendar", component: Calendar },
    { path: "/dashboard", name: "Dashboard", component: Dashboard },
    { path: "/:pathMatch(.*)*", name: "Not Found", component: NotFound },
  ],
});

app.use(createPinia());
app.use(ElementPlus);
app.use(router);

app.mount("#app");
