import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms"; // <-- NgModel lives here
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { RouterModule } from "@angular/router";

import { JeuComponent } from "./jeu/jeu.component";
import { ReglesComponent } from "./regles/regles.component";
import { NotFoundComponent } from "./404.component";

@NgModule({
  declarations: [AppComponent, JeuComponent, ReglesComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot([
      { path: "regles", component: ReglesComponent },
      { path: "jeu", component: JeuComponent },
      { path: "", redirectTo: "/regles", pathMatch: "full" },
      { path: "**", component: NotFoundComponent },
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
