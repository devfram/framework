import { createApp } from "vue";
import { createPinia } from "pinia";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";

import App from "./App.vue";
import NotFound from "./components/NotFound.vue";
import Calendar from "./components/Calendar-element.vue";
import Dashboard from "./components/Dashboard-element.vue";
import Subbands from "./components/real-time/Subbands-graph.vue";
import Beamlets from "./components/real-time/Beamlets-graph.vue";
import Crosslets from "./components/real-time/Crosslets-element.vue";
import { createRouter, createWebHashHistory } from "vue-router";
import "element-plus/theme-chalk/dark/css-vars.css";

import "./assets/main.css";

import ECharts from "vue-echarts";

const app = createApp(App);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    { path: "/", name: "Base", component: null },
    { path: "/calendar", name: "Calendar", component: Calendar },
    { path: "/dashboard", name: "Dashboard", component: Dashboard },
    { path: "/subbands", name: "Subbands", component: Subbands },
    { path: "/beamlets", name: "Beamlets", component: Beamlets },
    { path: "/crosslets", name: "Crosslets", component: Crosslets },
    { path: "/:pathMatch(.*)*", name: "Not Found", component: NotFound },
  ],
});

// import ECharts modules manually to reduce bundle size

app.use(createPinia());
app.use(ElementPlus);
app.use(router);
app.component("v-chart", ECharts);

app.mount("#app");
