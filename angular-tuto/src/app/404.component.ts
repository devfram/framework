import { Component } from "@angular/core";

@Component({
  selector: "jeu-element",
  template: `
    <br />
    <br />
    <h2>Page Not Found</h2>
    <p>We couldn't find that page! Not even with x-ray vision.</p>
  `,
  styles: [],
})
export class NotFoundComponent {}
