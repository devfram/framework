import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeamletsComponent } from './beamlets.component';

describe('BeamletsComponent', () => {
  let component: BeamletsComponent;
  let fixture: ComponentFixture<BeamletsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeamletsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BeamletsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
