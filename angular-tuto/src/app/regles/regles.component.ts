import { Component } from "@angular/core";

@Component({
  selector: "regles-element",
  template: `
    <div class="main">
      <h1>Regles du jeu</h1>
      <p>
        Le but du jeu est de deviner (guess) un nombre aléatoire entre 1 et 100.
        <br />
        10 essais sont disponibles pour trouver.
        <br /><br />
        Voici a quoi ressemble le jeu au début.
      </p>
      <img src="../../assets/jeu_debut.png" alt="jeu au lamcement" />
      <p>
        Une fois le guess entré, vous pouvez valider en appuyant sur la touche
        "Enter" ou en appuyant sur le boutton "Guess !". Le boutton "Reset"
        permet de recommencer une partie avec un nouveau nombre nimporte quand.
      </p>
      <p>Les coeurs représentent le nombre d'essais restants.</p>
      <img src="../../assets/jeu_3guess.png" alt="" />
      <p>
        Après chaque essais, la position du guess par rapoort au nombre est
        affiché ainsi que les essais précedents.
      </p>
      <p>Victoire :</p>
      <img src="../../assets/win.png" alt="" />
      <p>Défaite :</p>
      <img src="../../assets/loose.png" alt="" />
    </div>
  `,
  styles: [
    `
      p {
        font-size: large;
      }
      .main {
        margin: auto;
        width: 50%;
      }

      img {
        max-width: 963px;
        width: 150%;
        border-style: solid;
        border-color: rgb(2, 132, 199);
        border-width: 5px;
        border-radius: 5px;
      }

      h1 {
        color: #0248c7;
      }
    `,
  ],
})
export class ReglesComponent {}
