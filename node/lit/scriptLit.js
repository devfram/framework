import { html, css, LitElement } from 'lit';

export class GuessNum extends LitElement {
	static styles = css`
	.line {
		white-space: nowrap;
	}
	button {
		display: inline-block;
		background-color: transparent;
		border: solid;
		border-color: #505050;
		border-width: 2px;
		cursor: pointer;
	}
	
	:disabled {
		cursor: not-allowed;
	}
	sl-alert {
		position: absolute;
		display: inline;
		padding-left: 40%;
		width: 1200px;
	}
	sl-input {
		margin: 5px;
		display: inline-block;
		width: 30%;
	}`;

	static properties = {
		max: { type: Number },
		min: {},
		nbEssaisInit: {},
		nbEssais: {},
		histo: {},
		randNum: {},
		pos: {},
		status: {},
	};

	constructor() {
		super();
		this.max = 100;
		this.min = 1
		this.nbEssais = 10
		this.nbEssaisInit = this.nbEssais
		this.histo = "Guess précédents : "
		this.randNum = Math.floor(Math.random() * this.max + this.min);
		console.log(this.randNum);

	}

	takeGuess() {
		const guess = parseInt(this.shadowRoot.getElementById("guess").value);
		if (!guess) {
			const alert = this.shadowRoot.querySelector(`sl-alert`);
			alert.show();
			return;
		}
		console.log(guess);

		if (guess === this.randNum) {
			this.status = "win";
			console.log("victoire");
			return;
		}
		if (guess < this.randNum) {
			this.pos = "Trop bas";
		} else {
			this.pos = "Trop haut";
		}
		this.status = "miss";
		this.histo += guess + " ";
		this.nbEssais--;
		if (this.nbEssais === 0) {
			this.status = "loose";
		}

		this.shadowRoot.getElementById("guess").value = null;
	}

	reset() {
		this.randNum = Math.floor(Math.random() * this.max + this.min);
		this.shadowRoot.getElementById("guess").value = null;
		console.log(this.randNum);
		this.status = null;
		this.nbEssais = this.nbEssaisInit;
		this.histo = "Guess précédents : ";
	}

	isEnter(e) {
		if (e.code === "Enter") {
			this.takeGuess()
		}
	}

	firstUpdated() {
		const rating = this.shadowRoot.querySelector(".rating-hearts");
		rating.getSymbol = () => '<sl-icon name="heart-fill"></sl-icon>';
	}

	render() {

		return html`
		<sl-alert variant="warning" duration="3000" closable>
			<sl-icon slot="icon" name="exclamation-triangle"></sl-icon>
			<strong>Rentrez une valeur conforme</strong><br />
			Nombre entre 1 et 100
		</sl-alert>
		<div class="line">
			<sl-input type="number" @keyup=${this.isEnter} ?disabled=${this.status==="win" || this.status==="loose"} name="guess"
				id="guess" autofocus pill type="number" placeholder="ex : 53" label="Votre guess"></sl-input>
			<sl-button-group>
				<sl-button variant="success" outline id="valider" ?disabled=${this.status==="win" || this.status==="loose"}
					@click=${this.takeGuess}>Guess
					!</sl-button>
				<sl-button variant="warning" outline id="reset" @click=${this.reset}>Reset</sl-button>
			</sl-button-group>
		</div>
		<br />
		<h3 ?hidden=${this.status !="miss"} id="pos">${this.pos}</h3>
		<p ?hidden=${this.satus===null} id="histo">${this.histo}</p>
		<p ?hidden=${this.status !="loose"} id="loose">Dommage, le nombre était ${this.randNum}</p>
		<p ?hidden=${this.status !="win"} id="win">Bravo !</p>
		<sl-rating class="rating-hearts" style="--symbol-color-active: #ff4136" readonly value="${this.nbEssais}"
			max="${this.nbEssaisInit}">
		</sl-rating>
            `
	}
}
customElements.define('guess-num', GuessNum);