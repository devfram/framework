import { Component, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor() {}
  styles = 'position: fixed; cursor: pointer; width:88%';
  title = 'angular-dash';
  name = 'Jean Bébou';
  panelOpenState = false;

  h: Number | String = new Number();
  m: Number | String = new Number();
  s: Number | String = new Number();

  setTime() {
    setInterval(() => {
      const date = new Date();
      this.h = date.getUTCHours();
      this.m = date.getUTCMinutes();
      this.s = date.getUTCSeconds();
      if (this.s < 10) {
        this.s = '0' + this.s;
      }
      if (this.m < 10) {
        this.m = '0' + this.m;
      }
      if (this.h < 10) {
        this.h = '0' + this.h;
      }
    }, 1000);
  }

  ngAfterViewInit(): void {
    this.setTime();
  }

  changeWidth() {
    console.log('asfdasdf');
    console.log(!this.styles.includes('width'));

    if (!this.styles.includes('width')) {
      this.styles = 'position: fixed; cursor: pointer; width:88%';
      return;
    }
    this.styles = 'position: fixed; cursor: pointer;';
  }
}
