
//initialisation du jeu
var max = 100;
var min = 0;
var nbEssais = 10;
let randNum = Math.floor(Math.random() * max + min);
let histo = "Guess précédents : ";
console.log(randNum);
document.getElementById("guess").addEventListener("keypress", function (event) {
	if (event.key === "Enter") {
		takeGuess();
	}
});

//fonction principale du jeu
function takeGuess() {
	let guess = parseInt(document.getElementById("guess").value);
	if (!guess) {
		alert("Rentrez une valeur");
		return;
	}
	console.log(guess);
	if (guess === randNum) {
		document.getElementById("win").removeAttribute("style", "");
		document.getElementById("valider").setAttribute("disabled", "");
		document.getElementById("guess").setAttribute("disabled", "");
		document.getElementById("pos").setAttribute("style", "display:none;");
		return;
	}

	nbEssais--;
	document.getElementById("nbTryLeft").textContent = nbEssais;

	if (nbEssais > 0) {
		document.getElementById("pos").removeAttribute("style", "");
		if (guess < randNum) {
			document.getElementById("pos").textContent = "Plus haut";
		} else {
			document.getElementById("pos").textContent = "Plus bas";
		}

		histo += guess + " ";
		document.getElementById("histo").removeAttribute("style", "");
		document.getElementById("histo").textContent = histo;
		console.log(histo);
	}
	if (nbEssais === 0) {
		document.getElementById("valider").setAttribute("disabled", "");
		document.getElementById("guess").setAttribute("disabled", "");
		document.getElementById("loose").removeAttribute("style", "");
		document.getElementById("pos").setAttribute("style", "display:none;");
		document.getElementById("loose").textContent =
			"Perdu... le nombre était " + randNum;
		console.log("dis");
	}
	document.getElementById("guess").value = "";
}

//remise a 0 du jeu sans recharger
function reset() {
	histo = "Guess précédents : ";
	randNum = Math.floor(Math.random() * max + min);
	nbEssais = 10;
	document.getElementById("valider").removeAttribute("disabled", "");
	document.getElementById("guess").removeAttribute("disabled", "");
	document.getElementById("nbTryLeft").textContent = nbEssais;
	document.getElementById("win").setAttribute("style", "display:none;");
	document.getElementById("loose").setAttribute("style", "display:none;");
	document.getElementById("histo").setAttribute("style", "display:none;");
}
