// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Vuetify
import { createVuetify } from 'vuetify'
import colors from 'vuetify/lib/util/colors'

export default createVuetify({
  // https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides,
	theme: {
    defaultTheme: 'dark',
		light: {
			primary: colors.red.darken1, // #E53935
			secondary: colors.red.lighten4, // #FFCDD2
			accent: colors.indigo.base, // #3F51B5
		}, 
		dark: {
			secondary: colors.purple.darken4, // #FFCDD2
			accent: colors.indigo.base, // #3F51B5
		},
  },

})
